import {Component, Input, Output, EventEmitter} from '@angular/core';
import {ProductType} from './product';
import {CORE_DIRECTIVES, 
        FORM_DIRECTIVES, 
        FormBuilder, 
        ControlGroup, 
        Validators, 
        AbstractControl} from '@angular/common';

@Component ({
    selector: 'product-form',
    template: `
           <h1>Enter new product details</h1>
           <form [ngFormModel]="myForm" (ngSubmit)="onSubmit(myForm.value)">

                <div>Enter name: <input type="text"  [ngFormControl]="name"/></div>
                <div *ngIf="!name.valid && !myForm.pristine"><span style="color:red">Please enter the product name!</span></div>  

                <div>Enter price: <input type="text" [ngFormControl]="price"/></div>
                <div *ngIf="!price.valid && !myForm.pristine"><span style="color:red">Please enter the product price!</span></div>  

                <div>Enter image: <input type="text" [ngFormControl]="image"/></div> 
                <div *ngIf="!image.valid && !myForm.pristine"><span style="color:red">Please enter the product image!</span></div>  

                <div *ngIf="myForm.valid"><button type="submit">CREATE</button></div>
           </form> 
    `,
    directives: [CORE_DIRECTIVES, FORM_DIRECTIVES]
})
export class ProductFormComponent {
    @Input() productList: ProductType[];
    @Output() productListChange = new EventEmitter<ProductType[]>();

    myForm: ControlGroup;

    name: AbstractControl;
    price: AbstractControl;
    image: AbstractControl;

    constructor(fb: FormBuilder) {
        this.myForm = fb.group({
            name: ['', Validators.required],
            price: ['', Validators.required],
            image: ['', Validators.required]
        });

        this.name = this.myForm.controls['name'];
        this.price = this.myForm.controls['price'];
        this.image = this.myForm.controls['image'];
    }

    onSubmit(submitedValue: ProductType) {
        this.productList.push(submitedValue);
        this.productListChange.emit(this.productList);
    }
}