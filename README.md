# Writing applications in Angular 2 [part 8] #

## Building forms with FormBuilder ##

Last time in [part 7](https://bitbucket.org/tomask79/angular2-forms-intro) we builded form with help of [ngForm ](https://angular.io/docs/ts/latest/guide/forms.html#!#ngForm) directive. Where **ngForm** directive
was **automatically attached to the form tag** which gave us basically two things, ControlGroup object for the form and onSubmit hook. Eventually we defined
**implicitly** ngControls which were attached to the mentioned group. This approach is fine in the easy situations where form is static and you know it's 
structure from the beginning. **Well how about creating ControlGroup and Controls programatically?** This is the moment when **FormBuilder** comes in.

### Howto inject FormBuilder into Component ###

Very simply, you just need to import it and ask Angular 2 instance injector for the instance through **dependency injection**:

```
import {FormBuilder} from 'angular2/common';

export class ProductFormComponent {
   .
   .
   .

    constructor(fb: FormBuilder) {
        
    }
    .
    .
}
```
Now remember, FormBuilder gives us two things:

* we can **create ControlGroup manually** and not to use ControlGroup from ngForm directive.
* we can **manually add Controls** to the ControlGroup.

So let's try that, first we will modify my example of product form component to be creating name, price and image controls
manually:

```
export class ProductFormComponent {
    @Input() productList: ProductType[];
    @Output() productListChange = new EventEmitter<ProductType[]>();

    myForm: ControlGroup;

    name: AbstractControl;
    price: AbstractControl;
    image: AbstractControl;

    constructor(fb: FormBuilder) {
        this.myForm = fb.group({
            name: ['', Validators.required],
            price: ['', Validators.required],
            image: ['', Validators.required]
        });

        this.name = this.myForm.controls['name'];
        this.price = this.myForm.controls['price'];
        this.image = this.myForm.controls['image'];
    }
}
```
Now let's reference these controls in the template:

```
@Component ({
    selector: 'product-form',
    template: `
           <h1>Enter new product details</h1>
           <form [ngFormModel]="myForm" (ngSubmit)="onSubmit(myForm.value)">

                <div>Enter name: <input type="text"  [ngFormControl]="name"/></div>
                <div *ngIf="!name.valid && !myForm.pristine"><span style="color:red">Please enter the product name!</span></div>  

                <div>Enter price: <input type="text" [ngFormControl]="price"/></div>
                <div *ngIf="!price.valid && !myForm.pristine"><span style="color:red">Please enter the product price!</span></div>  

                <div>Enter image: <input type="text" [ngFormControl]="image"/></div> 
                <div *ngIf="!image.valid && !myForm.pristine"><span style="color:red">Please enter the product image!</span></div>  

                <div *ngIf="myForm.valid"><button type="submit">CREATE</button></div>
           </form> 
    `,
    directives: [CORE_DIRECTIVES, FORM_DIRECTIVES]
})
```
What we did in here:

* **[ngFormModel]="myForm"**, we're attaching here our manually created ControlGroup to the form. **Btw ngForm directive won't be attached by Angular 2 if ngFormModel is used!**
* **(ngSubmit)="onSubmit(myForm.value)**, we're setting up onSubmit hook here, where we're putting key[control name]/value[from control] pairs object to our onSubmit method.
* **[ngFormControl]="name", [ngFormControl]="price", [ngFormControl]="image" ** we're setting up reference to our manually created controls in the code. This has huge benefit, because our controls are accessible in the view as well as in the code! So we can check their state, values everywhere and not only in the view. This is the biggest difference compare to previous approach in part 7.

### Validations with FormBuilder ###

By default Angular 2 gives us following validations directive:

* Required
* Minlength
* MaxLength
* Pattern

Yes, you can create custom validations (I will test it in another part). Now let's try just **Required**.

```
<div *ngIf="!name.valid && !myForm.pristine"><span style="color:red">Please enter the product name!</span></div>
```

With this template code we're saying: Display error message only if **someone entered something into our form**, so our form IS NOT **pristine**
and name is not filled in, so it's not valid.

By template code:

```
<div *ngIf="myForm.valid"><button type="submit">CREATE</button></div>
```

we're annoucing that **button will be visible only if the WHOLE form is valid!**

# Testing the demo #

* git clone <this repo>
* npm install
* npm start
* visit http://localhost:3000

regards

Tomas


